#include <Wire.h>


char BytesReceived[17];// Store received Bytes
char BytesToSend[8];//Prepare Bytes to send 
float Lambdaist=0;
float Lambdasoll=1.05;
float RPMist=0;
float RPMsoll=3000;
float TVAist=45;
float TVMist=45;
boolean Mode=0;



void setup() {
  for (int i=0;i<17;i++){
    BytesReceived[i]=0;
  }
  for (int i=0;i<8;i++){
    BytesToSend[i]=0;
  }
  Wire.begin(12);                // join i2c bus with address #12
  Wire.onReceive(receiveEvent); // register event 
  Wire.onRequest(requestEvent); // 
  Serial.begin(9600);           // start serial for output
}

void loop() {
  delay(100);
}

// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany) {
  int i=0;
  while (1 < Wire.available()) { // loop through all but the last
    BytesReceived[i] = Wire.read(); // receive byte as a character
    i++;
  }
  ConvertArraytoValues();
}

void ConvertArraytoValues(){
  int b=0;
  unsigned long a=0;
  for (int i=b;i<4;i++){
    a=(a<<(i-b)*8)+BytesReceived[i];
  }
  Lambdaist=decasting(a);
  
  b=4;
  a=0;
  for (int i=b;i<8;i++){
    a=(a<<(i-b)*8)+BytesReceived[i];
  }
  Lambdasoll=decasting(a);
  
  b=8;
  a=0;
  for (int i=b;i<12;i++){
    a=(a<<(i-b)*8)+BytesReceived[i];
  }
  RPMist=decasting(a);
  
  b=12;
  a=0;
  for (int i=b;i<16;i++){
    a=(a<<(i-b)*8)+BytesReceived[i];
  }
  RPMsoll=decasting(a);

  if (BytesReceived[16]<1){
    Mode=0;
    }
    else{
      Mode=1;
    }
}

void ConvertValuestoArray(){
  unsigned long b=casting(TVAist);
  for (int i=0;i<4;i++){
    BytesToSend[i]=(b>>(24-i*8))&0xFF;
  }
  b=casting(TVMist);
  for (int i=4;i<8;i++){
    BytesToSend[i]=(b>>(24-(i-4)*8))&0xFF;
  }
}

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent() {
  
  Wire.write("hello "); // respond with message of 6 bytes
  // as expected by master
}




unsigned long casting (float a){
  unsigned long longo=a*100;
  return(longo);
}

float decasting (unsigned long a){
  float floato=((float) a)/100;
  return(floato);
}
