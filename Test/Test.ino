/*FINAL STATE MACHINE for controlling the engine of the woodgasifier
desgined for an arduino uno

author:     Dominik Felder, Balz Huber
date:       27.09.2018
version:    1.0

*/


//#include "functions.h"
//#include <Wire.h>
#include "Poti.h"

#define POTI1_INPUT A3           //analog input adress pot1
#define RANGE 100
//#define POTI2_INPUT A1          //analog input adress pot2

Poti Poti1(POTI1_INPUT,RANGE);
int time;



void setup(){

    //general setup
    Serial.begin(9600);
    pinMode(13,OUTPUT);

}



void loop(){ 
            Serial.print(Poti1.getPoti());
            Serial.print('\n');
            time = 100/Poti1.getPoti();
            
            digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
            delay(time);              // wait for a second
            digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
            delay(time);              // wait for a second

}


