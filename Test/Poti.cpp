/* cpp file with poti-class for the engine controller
author:     Dominik Felder, Balz Huber
date:       27.09.2018
version:    1.0
*/

#include "Poti.h"

//setup for potentiometer
    Poti::Poti(int poti_adr,int scale_range){
      range = scale_range;
      adr = poti_adr;
}

//get poti X values
//retruns pot_value from Poti X
//Balz
float Poti::getPoti(void){
    
    value = analogRead(adr);
    value = map(value,0,1023,0,range);
    value = constrain(value,0,range);
    return value;
            
}        
