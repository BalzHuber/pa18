#include <PID_v1.h>

//1. Klappe Ein- und Ausgänge definieren
#define KLAPPE1_INPUT A0
#define POTI1_INPUT A3
#define MOTOR1_OUTPUT 5

//2. Klappe Ein- und Ausgänge definieren
#define KLAPPE2_INPUT A2
#define POTI2_INPUT A1
#define MOTOR2_OUTPUT 3


////Gemeinsahme Variablen
int range = 100;
////Regelparameter
//

//Variablen für 1.Klappe
double Poti1;
int TPS1;
int Ref_max1;
int Ref_min1;
//Regler-Variablen für 1.Klappe
double Kp1=4, Ki1=0.1, Kd1=0.00;
double Setpoint1, Input1, Output1;
PID Controller1(&Input1, &Output1, &Setpoint1, Kp1, Ki1, Kd1, DIRECT);
int Force1 = 65;

//Variablen für 2.Klappe
double Poti2;
int TPS2;
int Ref_max2;
int Ref_min2;
//Regler-Variablen für 2.Klappe
double Kp2=3.5, Ki2=0.5, Kd2=0;
double Setpoint2, Input2, Output2;
PID Controller2(&Input2, &Output2, &Setpoint2, Kp2, Ki2, Kd2, DIRECT);
int Force2 = 70;
  

void setup () {

  Serial.begin(9600);
  TCCR3B = TCCR3B & B11111000 | B00000001;    // set timer 3 divisor to     1 for PWM frequency of 31372.55 Hz       //sets PWM to 32 KHz you really need this

  Ref_min1 = analogRead(KLAPPE1_INPUT);
  Ref_min2 = analogRead(KLAPPE2_INPUT);
  
  analogWrite(MOTOR1_OUTPUT,Force1);
  analogWrite(MOTOR2_OUTPUT,Force2);
  
  delay(10000);

  Ref_max1 = analogRead(KLAPPE1_INPUT);
  Ref_max1*=0.85;
  analogWrite(MOTOR1_OUTPUT,0);

  Ref_max2 = analogRead(KLAPPE2_INPUT);
  Ref_max2*=0.85;
  analogWrite(MOTOR2_OUTPUT,0);

  //turn the PID on
  Controller1.SetMode(AUTOMATIC);
  Controller2.SetMode(AUTOMATIC);
  
 
  //  pinMode(RPM_Pin,INPUT_PULLUP);
  Controller1.SetSampleTime(10);
  Controller2.SetSampleTime(10);
  
  }
  
  
  
void loop () {
//serialCheck();
// Erklärung zum Regler unter http://brettbeauregard.com/blog/2011/04/improving-the-beginners-pid-introduction/
  //Sollwert wird eingelesen und an Soll Regelwert übertragen
  Poti1 = analogRead(POTI1_INPUT);
 
  Poti1 = map(Poti1,0,1023,0,range);
  Poti1 = constrain(Poti1,0,range);
  Setpoint1 = Poti1;

  //Istwert wird eingetragen und an Regelstrecke übertragen
  //Referenzwert 0 wird vom TPS Signal von Anfang an abgezogen!
  TPS1 = analogRead(KLAPPE1_INPUT);
  
  TPS1 = map(TPS1,Ref_min1,Ref_max1,0,range);
  TPS1 = constrain(TPS1,0,range); 
  Input1 = TPS1;
  //Serial.println(Input);
  

  //Regler wird aufgerufen
  Controller1.Compute();
  //Output von Regler wird in einen Bereich eingeschränkt
  //Wenn Bereich vergrössert wird ist die Klappe schneller aber überschwingt eher
  Output1 = constrain(Output1,0,Force1);
  //PWM Signal wird auf Motor1_Output übergeben
  analogWrite(MOTOR1_OUTPUT, Output1);


  
  Poti2 = analogRead(POTI2_INPUT);
 
  Poti2 = map(Poti2,0,1023,0,range);
  Poti2 = constrain(Poti2,0,range);
  Setpoint2 = Poti2;

  //Istwert wird eingetragen und an Regelstrecke übertragen
  //Referenzwert 0 wird vom TPS Signal von Anfang an abgezogen!
  TPS2 = analogRead(KLAPPE2_INPUT);
  
  TPS2 = map(TPS2,Ref_min2,Ref_max2,0,range);
  TPS2 = constrain(TPS2,0,range); 
  Input2 = TPS2;
  //Serial.println(Input);
  

  //Regler wird aufgerufen
  Controller2.Compute();
  //Output von Regler wird in einen Bereich eingeschränkt
  //Wenn Bereich vergrössert wird ist die Klappe schneller aber überschwingt eher
  Output2 = constrain(Output2,0,Force2);
  //PWM Signal wird auf Motor2_Output übergeben
  analogWrite(MOTOR2_OUTPUT, Output2);
  /*
  Serial.print(Input1);
  Serial.print("  ");
  Serial.print(Setpoint1);
  Serial.print("  ");
  Serial.print(Output1);
  Serial.print("  ");
  Serial.print(Input2);
  Serial.print("  ");  
  Serial.print(Setpoint2);
  Serial.print("  ");
  Serial.println(Output2);
  */
}

