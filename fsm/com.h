/* header file with functions for the engine controller
author:     Dominik Felder, Balz Huber
date:       27.09.2018
version:    1.0
*/



#ifndef com_h
#define com_h

#include <Wire.h>
#include <arduino.h>
#include "functions.h"

//setup for communication between mega- and uno-board
void receiveEvent(value*);

void ConvertArraytoValues(value*);

void ConvertValuestoArray(float,float);

void requestEvent();

unsigned long casting(flaot);

float decasting(unsigned);

#endif

