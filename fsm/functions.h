/* header file with functions for the engine controller
author:     Dominik Felder, Balz Huber
date:       27.09.2018
version:    1.0
*/



#ifndef functions_h
#define functions_h

#include "PID_v1.h"
#include "ThrottleValve.h"
#include "Poti.h"
#include <Wire.h>


//define IN- and OUTPUTS
#define TVM_INPUT A0            //analog input adress throttle valve mix
#define MOTOR_TVM_OUTPUT 5      //analog output adress for throttle valve motor air

#define TVA_INPUT A2            //analog input adress throttle valve air
#define MOTOR_TVA_OUTPUT 3      //analog output adress for throttle valve motor air

#define POTI1_INPUT A3           //analog input adress pot1
#define POTI2_INPUT A1          //analog input adress pot2


#define MODE_INPUT D1           //digital input for mode

#define RANGE 100;              //Range for scale




struct value{
    float lambdaSet;
    float lambdaIn;
    float rpmSet;
    float rpmIn;
} typedef value;


//get state from IN ?
//returns state
int getState(void);

//setup for trottle valve
void setupTV(void);

    
//controller for lambda and rpm
//returns valve_position
float controllerLambda(float set, float input);
float controllerRpm(float set, float input);

#endif

