/* header file for a throttle valve object for the engine controllerauthor:     Dominik Felder, Balz Huber
date:       27.09.2018
version:    1.0
*/

#ifndef ThrottleValve_h
#define ThrottleValve_h

#include "PID_v1.h"


class ThrottleValve
{
    public:



        int force;
        int TPS;
        int Ref_max;
        int Ref_min;
        int range;

        double Kp, Ki, Kd;
        double Setpoint, Input, Output;

        int  adr_analog_input;
        int  adr_analog_output;
        
        PID controller;
        ThrottleValve(double*, double*, double*,        // * constructor.  links the PID to the Input, Output, and 
            double, double, double, int, int, int, int, int, int);      //Constructor
        
        void setTVposition(float);
        float getTVposition(void);

};


#endif
