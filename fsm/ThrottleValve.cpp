/* cpp file for a throttle valve object for the engine controller
author:     Dominik Felder, Balz Huber
date:       27.09.2018
version:    1.0
*/

#include "ThrottleValve.h"
#include <arduino.h>


//setup for ThrottleValve
ThrottleValve::ThrottleValve(double* Input, double* Output, double* Setpoint,
        double Kp, double Ki, double Kd, int POn, int ControllerDirection, int adr_analogInput, int adr_analogOutput, int range, int force)
        :controller(Input,Output,Setpoint,Kp,Ki,Kd,POn,ControllerDirection){
        
    adr_analog_input = adr_analogInput;
    adr_analog_output = adr_analogOutput;
}

//sets ThrottleValve positon
void ThrottleValve::setTVposition(float tv_position){
    Setpoint = tv_position;

    //Istwert wird eingetragen und an Regelstrecke übertragen
    //Referenzwert 0 wird vom TPS Signal von Anfang an abgezogen!
    TPS = analogRead(adr_analog_input);

    TPS = map(TPS,Ref_min,Ref_max,0,range);
    TPS = constrain(TPS,0,range);
    Input = TPS;

    //Regler wird aufgerufen
    controller.Compute();
    //Output von Regler wird in einen Bereich eingeschränkt
    //Wenn Bereich vergrössert wird ist die Klappe schneller aber überschwingt eher
    Output = constrain(Output,0,force);
    //PWM Signal wird auf Motor1_Output übergeben
    analogWrite(adr_analog_output, Output);
}

//gets ThrottleValve positon
float ThrottleValve::getTVposition(void){
    return analogRead(adr_analog_input);
     
}
