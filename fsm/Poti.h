/* header file with poti-class for the engine controller
author:     Dominik Felder, Balz Huber
date:       27.09.2018
version:    1.0
*/

#ifndef Poti_h
#define Poti_h

#include <arduino.h>

class Poti
{
    public:

    int range;
    int adr;
    float value;

    Poti(int poti_adr,int scale_range);      //Constructor
    
    float getPoti(void);

};


#endif
