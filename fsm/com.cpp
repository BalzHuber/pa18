/* cpp file for a communication to arduino MEGA
author:     Dominik Felder, Balz Huber
date:       27.09.2018
version:    1.0
*/


#include <Wire.h>
#include "functions.h"


char BytesReceived[17]  = {0};         // Store received Bytes
char BytesToSend[8]     =  {0};            // Prepare Bytes to send

bool Mode=0;

unsigned long casting (float a){
  unsigned long longo=a*100;
  return(longo);
}

float decasting (unsigned long a){
  float floato=((float) a)/100;
  return(floato);
}

void ConvertArraytoValues(value* data){
  unsigned long a=0;
  int b=4;
  
  for (int i=b;i<4;i++){
    a=(a<<(i-b)*8)+BytesReceived[i];
  }
  data->lambdaSet=decasting(a);
  
  
  a=0;
  for (int i=b;i<8;i++){
    a=(a<<(i-b)*8)+BytesReceived[i];
  }
  data->lambdaIn=decasting(a);
  
  b=8;
  a=0;
  for (int i=b;i<12;i++){
    a=(a<<(i-b)*8)+BytesReceived[i];
  }
  data->rpmIn=decasting(a);
  
  b=12;
  a=0;
  for (int i=b;i<16;i++){
    a=(a<<(i-b)*8)+BytesReceived[i];
  }
  data->rpmSet=decasting(a);

  if (BytesReceived[16]<1){
    Mode=0;
    }
    else{
      Mode=1;
    }
}

// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(value* p_parameters) {
  int i=0;
  while (1 < Wire.available()){ // loop through all but the last
    BytesReceived[i] = Wire.read(); // receive byte as a character
    i++;
  }
  ConvertArraytoValues(p_parameters);
}



void ConvertValuestoArray(float pos_tva, float pos_tvm){
  unsigned long b=casting(pos_tva);
  for (int i=0;i<4;i++){
    BytesToSend[i]=(b>>(24-i*8))&0xFF;
  }
  b=casting(pos_tvm);
  for (int i=4;i<8;i++){
    BytesToSend[i]=(b>>(24-(i-4)*8))&0xFF;
  }
}

// function that executes whenever data is requested by master
// this function is registered as an event, see setup()
void requestEvent() { 
  Wire.write(BytesToSend, 2);      // respond with message of BytesToSend array

}




