/*FINAL STATE MACHINE for controlling the engine of the woodgasifier
desgined for an arduino uno

author:     Dominik Felder, Balz Huber
date:       27.09.2018
version:    1.0

*/


#include "functions.h"
#include "com.h"
#include <Wire.h>

//
#define ADDRESS_MEGA 12


// Variables
//---------------------------------------------------
int state = 0;
volatile value Parameters;              // Struct where set and input values are stored
value* p_parameters = &Parameters;      // pointer to Parameters

Parameters.lambdaIn     = 0;
Parameters.lambdaSet    = 1.05;
Parameters.rpmIn        = 0;
Parameters.rpmSet       = 3000;

float outputLambda;
float outputRPM;

float TVAist=45;
float TVMist=45;

const byte emergencyStopPin = 2;

//---------------------------------------------------




void setup(){

//general setup
//Serial.begin(9600);
int TCCR3B = TCCR3B & B11111000 | B00000001;    // set timer 3 divisor to     1 for PWM frequency of 31372.55 Hz       //sets PWM to 32 KHz you really need this

//Setup for communication
Wire.begin(12);                 // join i2c bus with address #12
Wire.onReceive(receiveEvent(p_parameters));   // register event 
Wire.onRequest(requestEvent());   // 
Serial.begin(9600);


// Setup for ThrottleValve Class
setupTV();




//Emergency Stop Interrupt
pinMode(emergencyStopPin, INPUT);
attachInterrupt(digitalPinToInterrupt(emergencyStopPin), ISRemergencyStop, LOW);

}


void loop(){

    /*
    states:

    state 0 -> manuel mode
    state 1 -> automatic mode
    default -> manual mode
    */
    state = getState();

    switch (state)
    {
        case 0:
            
            // read Values from Potentiometers

            // set ThrottleValve to Position


            break;
        case 1:
            
            // calculate output Values for Lambda and RPM
            outputLambda = controllerLambda(Parameters.lambdaSet,Parameters.lambdaIn);
            outputRPM = controllerRpm(Parameters.rpmSet, Parameters.rpmIn);
            
            // set Throttle Valve
            setTVA(outputLambda);
            setTVM(outputRPM);


            // preparing data transfer to mega
            TVAist = getTVposition()
            TVMist = getTVposition()
            ConvertValuestoArray(TVAist,TVMist)
            
            break;
    
        default:

            state = 0; // default state -> manual mode
            
            break;
    }

}

// Interrupt Service Routine for Emergency Stop
void ISRemergencyStop(){

    // closes both Throttle Valves
    setTVA(0);
    setTVM(0);

}

