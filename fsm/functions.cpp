/* cpp file with functions for the engine controller
author:     Dominik Felder, Balz Huber
date:       27.09.2018
version:    1.0
*/

#include "functions.h"


//get state from IN ?
//returns state
int getState(void){

    return digitalRead(MODE_INPUT);
}

// setup throttle valves
void setupTV(){
    // Variables for Lambda controller
    double KpL=4, KiL=0.00, KdL=0.00; // only KdL 
    double SetpointL, InputL, OutputL;
    PID getPosLambda(&InputL, &OutputL, &SetpointL, KpL, KiL, KdL, DIRECT); 
    
    // Variables for RPM controller
    double KpR=4, KiR=0.00, KdR=0.00; // only KdL 
    double SetpointR, InputR, OutputR;
    PID getPosRPM(&InputR, &OutputR, &SetpointR, KpR, KiR, KdR, DIRECT);

  
    //Throttle Valve Air
    ThrottleValve TVA(&TVA.Input,&TVA.Output,&TVA.Setpoint,4,0.1,0.0,DIRECT,TVA_INPUT,MOTOR_TVA_OUTPUT);
    TVA.force = 65;

    //Throttle Valve Mix
    ThrottleValve TVM(&TVM.Input,&TVM.Output,&TVM.Setpoint,3.5,0.5,0,0,DIRECT,TVA_INPUT,MOTOR_TVA_OUTPUT);
    TVM.force = 70;

    TVA.Ref_min = analogRead(TVA_INPUT);
    TVM.Ref_min = analogRead(TVM_INPUT);
    
    analogWrite(MOTOR_TVA_OUTPUT,TVA.force);
    analogWrite(MOTOR_TVM_OUTPUT,TVM.force);
    
    delay(10000);

    TVA.Ref_max = analogRead(TVA_INPUT);
    TVA.Ref_max*=0.85;
    analogWrite(MOTOR_TVA_OUTPUT,0);

    TVM.Ref_max = analogRead(TVM_INPUT);
    TVM.Ref_max*=0.85;
    analogWrite(MOTOR_TVM_OUTPUT,0);

    //turn the PID on
    getPosRPM.SetMode(AUTOMATIC);
    getPosLambda.SetMode(AUTOMATIC);
    TVA.controller.SetMode(AUTOMATIC);
    TVM.ontroller.SetMode(AUTOMATIC);
    
    //  pinMode(RPM_Pin,INPUT_PULLUP);
    getPosLambda.SetSampleTime(10);
    getPosRPM.SetSampleTime(10);
    TVA.controller.SetSampleTime(10);
    TVM.ontroller.SetSampleTime(10);
    
}

    
//controller for lambda and rpm
//returns set value for valve_position
float controllerLambda(float set, float input){

    InputL      = input;
    SetpointL   = set;
    
    getPosLambda.Compute();

    return OutputL;
}

float controllerRpm(float set, float input){
    
    InputR      = input;
    SetpointR   = set;
    
    getPosRPM.Compute();

    return OutputR;
}
